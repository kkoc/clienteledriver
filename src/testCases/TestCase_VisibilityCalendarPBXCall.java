package testCases;

import org.testng.annotations.Test;

import utility.Constant;

public class TestCase_VisibilityCalendarPBXCall extends RunTest
{
	/***
	 * Testowanie widocznosci kalendarza w polu "Oczekiwany termin uruchomienia uslugi dla klienta:"
	 * w zgloszeniach PBX Request.
	 */
	@Test
	public void VisibilityCalendarPBXCall_Test() throws Exception
	{		
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Testowanie widocznosci kalendarza w polu 'Oczekiwany termin uruchomienia' w zgloszeniach PBX Request");
		action.logIn(env);
		action.findRealization(Constant.REALIZATION_ID);
		action.call.addNewCall("Kreacja i konfiguracja PBX'a", 1);
		
		action.cliDriver.findElementByXpath("//*[contains(text(),'Termin uruchomienia')]/following::input[1]").clear();
		try
		{
			action.cliDriver.findElementByXpath("//div[@class='xdsoft_datetimepicker xdsoft_noselect xdsoft_']").isDisplayed();
			Thread.sleep(1000);
			action.cliDriver.reportPass("Kalendarz umozliwiajacy wybor oczekiwanego terminu zakonczenia zostal wyswietlony",true);
		}
		catch (Exception e)
		{
			action.cliDriver.reportFail("Kalendarz umozliwiajacy wybor oczekiwanego terminu zakonczenia zostal wyswietlony", true);
		}
		
	}
}
