package testCases;

import org.testng.annotations.Test;
import utility.Constant;

public class TestCase_DeleteAllPossibleCalls extends RunTest
{
	/***
	 * Test usuwajacy ze zlecen technicznych te calle, ktore da sie usunac
	 */
	@Test
	public void DeleteAllPossibleCalls()
	{
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test usuwajacy ze zlecen technicznych te calle, ktore da sie usunac");
		action.logIn(env);
		
		action.findRealization(Constant.REALIZATION_ID);
		action.cliDriver.linkClickByName("Zlecenia Techniczne");
		action.call.deleteCalls();		
		action.cliDriver.reportPass("Dolaczono screenshot",true);	
	}

}
