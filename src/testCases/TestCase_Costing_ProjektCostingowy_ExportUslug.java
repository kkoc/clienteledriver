package testCases;

import org.testng.annotations.Test;

public class TestCase_Costing_ProjektCostingowy_ExportUslug extends RunTest 
{
	/***
	 * Test wyszukujący projekt Costingowy i klikajacy przycisk Export to Excel
	 */
	@Test
	public void Costing_CostProject_ExportToExcel()
	{
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test wyszukujący projekt Costingowy i klikajacy przycisk Export to Excel");
		action.logIn(env);
		action.cliDriver.clickMainTabLinkByName("Costing");
		action.cliDriver.textFieldSetTextByLabel("Numer wyceny Clientele", "117");
		action.cliDriver.selectItemFromSpecialListById("AssignedTo_chosen", "wszyscy");
		action.cliDriver.buttonClickByName("Szukaj");
		action.cliDriver.clickElementByXPath("//*[@id='tableProjects']//td[text()='117']");
		action.cliDriver.verifyElementVisibleByName("Szczegóły projektu Costingowego", 
				"Ekran 'Szczegoly projektu Costingowego' zostal wyswietlony", 
				"Ekran 'Szczegoly projektu Costingowego' NIE zostal wyswietlony");
		action.cliDriver.buttonClickByName("Export To Excel");
		action.cliDriver.reportPass("Pobrano plik");
		//action.cliDriver.reportPass("Dolaczono screenshot",true);	
	}

}
