package testCases;

import org.testng.annotations.Test;

import utility.Constant;

public class TestCase_AddFindDelete_Call extends RunTest
{
	String idCall;
	
	/***
	 * Test utworzenia nowego call'a na zakladce zlecenia techniczne
	 */
	@Test
	public void AddNewCall()
	{
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test utworzenia nowego call'a na zakladce zlecenia techniczne");
		action.logIn(env);
		action.findRealization(Constant.REALIZATION_ID); //przykladowe id	
		//action.cliDriver.linkClickByName("Zlecenia Techniczne");
		action.call.addNewCall("Zam�wienie transmisji GENERIC", 1);
		idCall= action.cliDriver.findElementById("_ctl1__ctl0_lblCallNum").getText();
		
		action.cliDriver.reportPass("Dolaczono screenshot",true);
	
	}
	
	/***
	 * Test wyszukiwania zgloszenia po CallID
	 */
	@Test(priority=1)
	public void FindCall()
	{
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test wyszukiwania zgloszenia po CallID utworzonego w poprzednim tescie");
		action.logIn(env);
		action.call.findCall(idCall); //przykladowe id				
		action.cliDriver.reportPass("Dolaczono screenshot",true);	
	}
	
	/***
	 * Test usuwajacy ze zlecen technicznych call o podanym numerze
	 */
	@Test(priority=2)
	public void DeleteCall()
	{
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), " Test usuwajacy ze zlecen technicznych call utworzony w poprzednim tescie ");
		action.logIn(env);		
		action.findRealization(Constant.REALIZATION_ID);
		action.cliDriver.linkClickByName("Zlecenia Techniczne");
		action.cliDriver.clickElementByXPath("//*[text()='"+idCall+"']/ancestor::tr[1]/descendant::input[@value='Delete'][1]");
		//find call:
		action.cliDriver.clickMainTabLinkByName("Realizacje");
		action.cliDriver.enterTextById("callno", idCall);
		action.cliDriver.clickElementByXPath("//input[@id='callno']/following::button[1]");
		action.cliDriver.verifyElementVisibleByName("Wprowadzono b��dny nr calla", "Usunieto call "+idCall, "Nie udalo sie usunac call "+idCall);
		
		action.cliDriver.reportPass("Dolaczono screenshot",true);	
	}
	
}
