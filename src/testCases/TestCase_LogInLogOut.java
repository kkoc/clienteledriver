package testCases;

import org.testng.annotations.Test;

public class TestCase_LogInLogOut extends RunTest
{
	/***
	 * Test loguj�cy si� do Clientele
	 */
	@Test
	public void LogIn()
	{
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test zalogowania sie z Clientele");
		action.logIn(env);
		action.cliDriver.reportPass("Dolaczono screenshot",true);
	
	}
	/***
	 * Test wylogowuj�cy si� z Clientele
	 */
	@Test
	public void LogOut()
	{
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test wylogowania sie z Clientele");
		action.logIn(env);
		action.logOut();
		action.cliDriver.reportPass("Dolaczono screenshot",true);
	}
	

}
