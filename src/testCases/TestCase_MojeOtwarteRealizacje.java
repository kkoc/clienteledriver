package testCases;

import org.testng.annotations.Test;

import utility.Constant;

public class TestCase_MojeOtwarteRealizacje extends RunTest
{
	/***
	 * Redmine #748 - Realizacje - moje otwarte - File or directory not found
	 * Sposob wywolania bledu:
	 * 1. Logujemy sie do aplikacji
	 * 2. Klikamy link Administracja w menu gornym
	 * 3. Klikamy Powrot do Clientele
	 * 4. Klikamy Realizacje -> Moje otwarte
	  * Strona sie nie laduje.
	  * @author Dominik Jabłoński
	 */
	@Test
	public void MojeOtwarteRealizacje_Test()
	{		
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Opis Testu");
		//1:
		action.logIn(env);
		//2:
		if(action.cliDriver.isElementVisibileByName("Administracja"))
		{
			action.cliDriver.clickMainTabLinkByName("Administracja");
		}else
		{
			action.cliDriver.clickMainTabLinkByName("Administration");
		}
		//3:
		action.cliDriver.clickMainTabLinkByName("Powr�t do Clientele");
		//4:
		action.cliDriver.hoverMainTabLinkByName("Realizacje");
		
        //a.moveToElement(action.cliDriver.driver.findElement(By.xpath("//*[@id='topNavUl']/descendant::*[text()='Realizacje'] | //*[@id='topNavUl']/descendant::*[@value='Realizacje']"))).build().perform();
        action.cliDriver.linkClickByName("Moje otwarte");	
        action.cliDriver.verifyElementVisibleByXpath("//*[@id='AssignedTo_chosen']/descendant::*[contains(text(),'"+Constant.USERNAME.toUpperCase()+"')]", "Zaladowana strona 'Moje otwarte'", "trona sie nie laduje");
		action.cliDriver.reportPass("Dolaczono screenshot",true);	
	}	

}
