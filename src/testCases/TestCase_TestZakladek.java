package testCases;

import org.testng.annotations.Test;

public class TestCase_TestZakladek extends RunTest
{
	/***
	 * Test przechodz�cy przez wszystkie g��wne zak�adki systemu Clientele.
	 */
	@Test
	public void ShowAllTabs()
	{
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test wyswietlenia kazdej z zakladek");
		action.logIn(env);
		//Klienci
		action.cliDriver.clickMainTabLinkByName("Klienci");
		action.cliDriver.waitForElementByText("Wyszukiwarka Klient�w");
		action.cliDriver.reportPass("Wyswietlono zakladke 'Klienci'");		
		//Realizacje
		action.cliDriver.clickMainTabLinkByName("Realizacje");
		action.cliDriver.waitForElementByText("Wyszukiwanie Realizacji");
		action.cliDriver.reportPass("Wyswietlono zakladke 'Realizacje'");	
		//Kolejki
		action.cliDriver.clickMainTabLinkByName("Kolejki");
		action.cliDriver.waitForElementByText("Wyszukiwanie Calli");
		action.cliDriver.reportPass("Wyswietlono zakladke 'Kolejki'");		
		//CRM
		action.cliDriver.clickMainTabLinkByName("CRM");
		action.cliDriver.waitForElementByText("Wyszukiwarka Quot");
		action.cliDriver.reportPass("Wyswietlono zakladke 'CRM'");
		//Costing
		action.cliDriver.clickMainTabLinkByName("Costing");
		action.cliDriver.waitForElementByText("Lista projekt�w Costingowych");
		action.cliDriver.reportPass("Wyswietlono zakladke 'Costing'");
		//Administration/Administracja
		//na zakladce Costing nazwa zakladki jest po angielsku - Administration
		action.cliDriver.clickMainTabLinkByName("Administration");
		action.cliDriver.waitForElementByText("Panel Administracyjny");
		action.cliDriver.reportPass("Wyswietlono zakladke 'Administration'");
		
		action.cliDriver.reportPass("Wyswietlono wszystkie zakladki");
	}

}
