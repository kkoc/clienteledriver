package testCases;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import utility.ClienteleDriver;
import utility.DriverActions;


public class RunTest {

	String env="DEV";
	DriverActions action;
		
	@BeforeMethod
	public void beforeMethod() throws Exception 
	{
		ClienteleDriver cliDriver= new ClienteleDriver();
		action=new DriverActions(cliDriver);			
	}	
//	//-------------------------------------------------------------------Szablon start!
//	/***
//	 * Opis Testu
//	 */
//	@Test
//	public void szablon_NowyTest()
//	{
//		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Opis Testu");
//		action.logIn(env);
//		//
//		//Tre�� testu
//		//
//		action.cliDriver.reportPass("Dolaczono screenshot",true);	
//	}	
//	//-------------------------------------------------------------------Szablon end!	
	@AfterMethod
	public void afterMethod(ITestResult result)
	{	 
		if(ITestResult.FAILURE==result.getStatus())
		{
			action.getCliDriver().reportFail("Zakonczono failem", false);
		}
		action.quit();
	}
	
}
