package testCases;

import org.testng.annotations.Test;

import utility.Constant;

public class TestCase_ProccesingCalls extends RunTest 
{
	/***
	 * Test tworzacy nowy call GENERIC w wybranym projekcie i przechodzacy kolejne etapy procesu.
	 */
	@Test
	public void ProcessingCall_Generic_Test() throws Exception
	{		
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test tworzacy nowy call GENERIC w wybranym projekcie i przechodzacy kolejne etapy procesu.");
		action.logIn(env);
		action.findRealization(Constant.REALIZATION_ID);
		action.call.addNewCall("Zam�wienie transmisji GENERIC", 1);
		action.call.fillNewCall_Generic(".WROCLAW - .WROCLAW FERA", "40000");	
		action.call.przekazCallDoRealizacji(); 
		action.call.przejmijZgloszenie();
		action.call.przekazCallDoPodwykonawcy();
		action.call.przekazCallDoDoneZainstalowano();
		action.call.ZrealizujCall("Generic");	
		
		action.cliDriver.reportPass("Zrealizowano",true);	
	}
	
	/***
	 * Test tworzacy nowy call RL w wybranym projekcie i przechodzacy kolejne etapy procesu.
	 */
	@Test
	public void ProcessingCall_RL_Test() throws Exception
	{		
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test tworzacy nowy call RL w wybranym projekcie i przechodzacy kolejne etapy procesu.");
		action.logIn(env);
		action.findRealization(Constant.REALIZATION_ID);
		action.call.addNewCall("Zam�wienie transmisji RL II", 1);
		action.call.fillNewCall_RL("4000", "Marconi - 28GHz", "Ethernet", "00 00 00.0", "00 00 00.0", "Magnus", "6969");	
		action.call.przekazCallDoRealizacji();
		action.call.przejmijZgloszenie();
		action.call.przekazCallDoPodwykonawcy();
		action.call.przekazCallDoDoneZainstalowano();
		action.call.ZrealizujCall("RL");
		
		action.cliDriver.reportPass("Zrealizowano",true);	
	}
	
	/***
	 * Test tworzacy nowy call ZUI w wybranym projekcie i przechodzacy kolejne etapy procesu.
	 */
	@Test
	public void ProcessingCall_ZUI_Test() throws Exception
	{		
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test tworzacy nowy call ZUI w wybranym projekcie i przechodzacy kolejne etapy procesu.");
		action.logIn(env);
		action.findRealization(Constant.REALIZATION_ID);
		action.call.addNewCall("Zam�wienie Us�ugi Internetowej II", 1);
		action.call.fillNewCall_ZUI(".WROCLAW FERA - .WROCLAW", "4000");
		action.call.przekazCallDoRealizacji();
		action.call.przejmijZgloszenie();		
		String idCall= action.cliDriver.findElementById("_ctl1__ctl0_lblCallNum").getText();
		action.cliDriver.buttonClickByName("Przyznano adresy");
		action.call.findCall(idCall);
		action.call.przejmijZgloszenie();
		action.call.ZrealizujCall("ZUI");		
		
		action.cliDriver.reportPass("Zrealizowano",true);
		
	}
		
	/***
	 * Test tworzacy nowy call PBX w wybranym projekcie i przechodzacy kolejne etapy procesu.
	 */
	@Test
	public void ProcessingCall_PBX_Test() throws Exception
	{		
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test tworzacy nowy call PBX w wybranym projekcie i przechodzacy kolejne etapy procesu");
		action.logIn(env);
		action.findRealization(Constant.REALIZATION_ID);
		action.call.addNewCall("Kreacja i konfiguracja PBX'a", 1);
		action.call.fillNewCall_PBX("123"+action.randomNr(10, 99), "2016-12-31", "BROAD_SOFT", "KONTO", "Internet", "Bramka");
		action.call.processCall_PBX();
		action.cliDriver.reportPass("Zrealizowano",true);
		
	}
	
	
	/***
	 * Test tworzacy nowy call 'Deinstalacja Transmisji (obs�uga Radio,SDH,OS,Gene' w wybranym projekcie i przechodzacy kolejne etapy procesu.
	 */
	@Test
	public void ProcessingCall_Deinstallation_Test() throws Exception
	{		
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test tworzacy nowy call 'Deinstalacja Transmisji (obs�uga Radio,SDH,OS,Gene' w wybranym projekcie i przechodzacy kolejne etapy procesu.");
		action.logIn(env);
		action.findRealization(Constant.REALIZATION_ID);
		action.call.addNewCall("Deinstalacja Transmisji (obs�uga Radio,SDH,OS,Gene", 1);
		action.call.fillNewCall_Deinstallation(".WROCLAW - .WROCLAW FERA");	
		action.call.przekazCallDoRealizacji();
		action.call.przejmijZgloszenie();
		action.call.przekazCallDoPodwykonawcy();
		action.cliDriver.checkboxClickByID("DeinstallationYes");
		action.call.ZrealizujCall("deinst");
		
		action.cliDriver.reportPass("Zrealizowano",true);	
	}

}
