package testCases;

import org.testng.annotations.Test;

public class TestCase_LinkStareNoweRealizacje extends RunTest 
{
	/***
	 * Test sprawdzajacy czy przelaczanie sie pomiedzy 'Stare realizacje' i 'Nowe realizacje' nie powoduje bledu.
	 */
	@Test
	public void LinkStareNoweRealizacje_Test()
	{
		action.cliDriver.runRaporting(Thread.currentThread().getStackTrace()[1].getMethodName(), "Test sprawdzajacy czy przelaczanie sie pomiedzy 'Stare realizacje' i 'Nowe realizacje' nie powoduje bledu.");
		action.logIn(env);
		action.cliDriver.clickMainTabLinkByName("Realizacje");
		action.cliDriver.linkClickByName("Stare realizacje");
		action.cliDriver.verifyElementVisibleByName("Nowe realizacje", "Przelaczono sie na 'Stare realizacje'"," Nie udalo sie przelaczyc na 'Stare realizacje'");
		action.cliDriver.linkClickByName("Nowe realizacje");
		action.cliDriver.verifyElementVisibleByName("Stare realizacje", "Przelaczono sie na 'Nowe realizacje'"," Nie udalo sie przelaczyc na 'Nowe realizacje'");
		action.cliDriver.reportPass("Zakonczono pozytywnie");
	}

}
