package utility;

import java.util.Random;

public class DriverActions {
	
	public ClienteleDriver cliDriver;
	public Actions_Call call;
	
	public DriverActions(ClienteleDriver _cliDriver) 
	{
		setCliDriver(_cliDriver);
		call = new Actions_Call(cliDriver);
	}
	public ClienteleDriver getCliDriver() 
	{
		return cliDriver;
	}
	public void setCliDriver(ClienteleDriver cliDriver) 
	{
		this.cliDriver = cliDriver;
	}
	private static String selectEnv(String env) 
	{
		String URL = null;
		switch (env.toLowerCase()) {
		case "dev":
			URL = Constant.ENV_URL_DEV;
			break;
		case "virt":
			URL = Constant.ENV_URL_VIRT;
			break;
		default:
			URL = Constant.ENV_URL_DEV;
		}
		return URL;
	}
	public void quit()
	{
		getCliDriver().quit();
	}
	public int randomNr (int start, int end)
	{
		Random r = new Random();		
		return (r.nextInt(end-start)+start);
	}
	//--------------------
	/***
	 * Metoda logujaca uzytkownika w Clientele
	 * @param environment - example "dev","virt"
	 */
	public void logIn(String environment)
	{
		String URL = selectEnv(environment);		
		getCliDriver().driver.get(URL);	
		
		cliDriver.verifyElementVisibleByName("Logowanie", "Wyswietlono strone logowania", "Strona logowania nie zostala wyswietlona");
		cliDriver.textFieldSetTextByLabel("Login name:", Constant.USERNAME);
		cliDriver.textFieldSetTextByLabel("Password:", Constant.PASSWORD);
	    cliDriver.radioButtonClickByName("Autoryzacja Domenowa (NGKTPL)");
	    cliDriver.buttonClickByName("Login"); 		    
        cliDriver.waitForElementByText("Klienci");//Constant.USERNAME); 
        cliDriver.reportPass("Zalogowano do srodowiska "+environment);
	}
	/***
	 * Metoda wylogowujaca uzytkownika z Clientele
	 */
	public void logOut()
	{
//		cliDriver.clickElementByName(Constant.USERNAME);		
		cliDriver.clickElementByXPath("//span[contains(@class,'glyphicon-user')]");
		if(cliDriver.isElementVisibileByName("Wyloguj"))
		{
			cliDriver.linkClickByName("Wyloguj");
		}
		if (cliDriver.isElementVisibileByName("Logowanie")==false)
		{
			cliDriver.hoverElementByXPath("//span[contains(@class,'glyphicon-user')]");
	        cliDriver.linkClickByName("Wyloguj");	        
		}
		
		cliDriver.waitForElementByText("Logowanie");
		cliDriver.reportPass("Wylogowano z Clientele");	
	}
	
	/***
	 * Metoda wyszukujaca realizacje.
	 * @param realizationID
	 */
	public void findRealization(String realizationID)
	{
		cliDriver.clickMainTabLinkByName("Realizacje");
		cliDriver.waitForElementByText("Wyszukiwanie Realizacji");
		cliDriver.textFieldSetTextByLabel("Realizacja", realizationID);
		cliDriver.buttonClickByName("Szukaj");
		cliDriver.linkClickByName(realizationID);
		cliDriver.waitForElementByText("Szczeg�y Projektu");		
		cliDriver.verifyElementVisibleByName("Szczeg�y Projektu", "Projekt o ID='" + realizationID + "' zostal odnaleziony", "Nie udalo sie odnalezc projektu o ID='" + realizationID);
	}

	
	
	//-------------------
	


	
	
}
