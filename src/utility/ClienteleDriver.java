package utility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

public class ClienteleDriver 
{	
	public WebDriver driver;
	protected static DesiredCapabilities dCaps; //?
	public String screenshotFilename;
	public ExtentReports extent;
	public ExtentTest test;
	
	private String currentTestName;
	
	
	public ClienteleDriver()
	{		
		extent = ExtentManager.GetExtent();		
		try 
		{
			System.setProperty("webdriver.chrome.driver",Constant.CHROMEDRIVERLOCATION);
			//--------wylaczenie komunikatow o zapisywaniu hasel:
			 ChromeOptions options = new ChromeOptions();
			 Map<String, Object> prefs = new HashMap<String, Object>();
//			 prefs.put("profile.default_content_settings.popups", 0);
//			 prefs.put("download.default_directory", Constant.PATH_DOWNLOAD);
			 prefs.put("credentials_enable_service", false);
			 prefs.put("profile.password_manager_enabled", false);
			 options.setExperimentalOption("prefs", prefs);
			//--------			
			driver = new ChromeDriver(options);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Constant.DEFAULT_TIMEOUT, TimeUnit.SECONDS);
		} 
		catch (Exception e) 
		{
			reportFail("Problem z utworzeniem drivera", false);
			reportFail("Exception desc : " + e.getMessage(), false);
		}
		currentTestName="";
	}
	/**
	 * Metoda uruchamiajaca raportowanie dla danego testu
	 * @param testName - nazwa testu
	 * @param testDecription - opis testu
	 */
	public void runRaporting(String testName, String testDecription)
	{
		test = extent.createTest(testName, testDecription);
		currentTestName=testName;
	}
	
	//----------------reporting:
	/**
	 * Metoda raportujaca Pass - poprawne wykonanie kroku
	 * @param description - opis
	 */
	public void reportPass(String description)
	{
		reportPass(description, false);
	}
	/**
	 * Metoda raportujaca Pass - poprawne wykonanie kroku
	 * @param description - opis
	 * @param czyScreenshot - wybor czy ma zostac dolaczony zrzut ekranu
	 */
	public void reportPass(String description, Boolean czyScreenshot)
	{
		if(czyScreenshot==true)
		{			
			try 
			{				
				test.pass(description, MediaEntityBuilder.createScreenCaptureFromPath(takeScreenshot("passed")).build());
			} 
			catch (IOException e) 
			{
				System.out.println("Exception while taking screenshot "+e.getMessage());
			}
		}
		else
		{
			test.pass(description);
		}
	}	
	/**
	 * Metoda raportujaca Fail - niepoprawne wykonanie kroku
	 * @param description - opis
	 * @param czyExit - wybor czy ma test ma zostac zakonczony
	 */
	public void reportFail(String description, Boolean czyExit)
	{				
		try
		{
			test.fail(description, MediaEntityBuilder.createScreenCaptureFromPath(takeScreenshot()).build());
		}
		catch (Exception e)
		{				
			System.out.println("Exception while taking screenshot "+e.getMessage());
		}
		
		if (czyExit==true)
		{
			quit();
			throw new AssertionError("Wywolano zamkniecie testu");
		}
	}
	/**
	 * Metoda wykonujaca zrzut ekranu z domyslna nazwa screena - "failed"
	 */
	public String takeScreenshot ()
	{
		return takeScreenshot("failed");
	}
	/**
	 * Metoda wykonujaca zrzut ekranu
	 * @param description - opis dolaczany do nazwy screena
	 */
	public String takeScreenshot (String description)
	{
		try 
		{
			File srcFile= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			screenshotFilename = currentTestName+"_"+ actualDate()+"_"+description+".png";		
			FileUtils.copyFile(srcFile, new File(Constant.PATH_SCREENSHOT+screenshotFilename+".png"));
		}
		catch (IOException ioe)		 
		{
			reportFail("Nie udalo sie wykonac screenshot. Komunikat ="+ ioe.getMessage(), false);
			System.out.println("Exception while taking screenshot "+ioe.getMessage());
		}
		
		return Constant.PATH_SCREENSHOT+screenshotFilename+".png";
	}
	//-------------------------
	/**
	 * Metoda statyczna zwracajaca aktualna date w formacie "yyyy-MM-dd_HH.mm.ss"
	 * @return String
	 */
	public static String actualDate() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	
	//--------------------------find Element:
	/**
	 * Metoda wyszukujaca element o podanej sciezce XPath
	 * @param xpath
	 * @return WebElement
	 */
	public WebElement findElementByXpath(String XPathValue)
	{
		WebElement element=null;
		try
		{			
			element = driver.findElement(By.xpath(XPathValue));
		}
		catch(NoSuchElementException nsee)
		{
			reportFail("Nie odznaleziono elementu o xpath= "+XPathValue,false);
		}			
		if (element.equals(null))
		{
			reportFail("Nie odznaleziono elementu o xpath= "+XPathValue,false);
		}
		else
		{
			ScrollToView(element);
		}
		
		return element;
	}
	/**
	 * Metoda wyszukujaca element o podanym Id
	 * @param id - id elementu
	 * @return WebElement
	 */
	public WebElement findElementById(String IdValue)
	{
		WebElement element=null;
		try
		{
			element = driver.findElement(By.id(IdValue));
		}
		catch(NoSuchElementException nsee)
		{
			reportFail("Nie odznaleziono elementu o id= "+IdValue,false);
		}			
		if (element.equals(null))
		{
			reportFail("Nie odznaleziono elementu o id= "+IdValue,false);
		}
		else
		{
			ScrollToView(element);
		}
	
		return element;
	}
	//--------------------------verifyElementVisible
	/**
	 * Metoda weryfikujaca czy element jest widoczny na stronie. Raportuje wynik.
	 * @param findByWhat - selektor typu wyszukiwania - example: "id", "name", "xpath"
	 * @param value - wartosc po ktorej wyszukiwany jest element
	 * @param komunikatTrue - tresc komunikatu gdy element istnieje.
	 * @param komunikatFalse - tresc komunikatu gdy element NIE istnieje.
	 */
	private void verifyElementVisibleBy(String findByWhat, String value, String komunikatTrue, String komunikatFalse)
	{
		WebElement element;
		switch(findByWhat.toLowerCase())
		{
			case "id":
			{
				element=findElementById(value);
				break;
			}
			case "name":
			{
				element=findElementByXpath("//*[contains(text(),'"+value+"')]");
				break;
			}
			case "xpath":
			default:
			{
				element=findElementByXpath(value);
				break;
			}		
		}
		if (element.equals(null))
		{
			reportFail(komunikatFalse,true);
		}	
		else
		{
			reportPass(komunikatTrue);
		}		
	}
	/**
	 * Metoda weryfikujaca czy element o podanym Id jest widoczny na stronie. Raportuje wynik.
	 * @param id - id elementu
	 * @param komunikatTrue - tresc komunikatu gdy element istnieje.
	 * @param komunikatFalse - tresc komunikatu gdy element NIE istnieje.
	 */
	public void verifyElementVisibleById(String value, String komunikatTrue, String komunikatFalse)
	{
		verifyElementVisibleBy("id", value, komunikatTrue, komunikatFalse);
	}
	/**
	 * Metoda weryfikujaca czy element o podanej sciezke XPath jest widoczny na stronie. Raportuje wynik.
	 * @param xpath
	 * @param komunikatTrue - tresc komunikatu gdy element istnieje.
	 * @param komunikatFalse - tresc komunikatu gdy element NIE istnieje.
	 */
	public void verifyElementVisibleByXpath(String value, String komunikatTrue, String komunikatFalse)
	{
		verifyElementVisibleBy("xpath", value, komunikatTrue, komunikatFalse);
	}
	/**
	 * Metoda weryfikujaca czy element o podanej nazwie jest widoczny na stronie. Raportuje wynik.
	 * @param name - nazwa elementu
	 * @param komunikatTrue - tresc komunikatu gdy element istnieje.
	 * @param komunikatFalse - tresc komunikatu gdy element NIE istnieje.
	 */
	public void verifyElementVisibleByName(String name, String komunikatTrue, String komunikatFalse)
	{
		verifyElementVisibleBy("name", name, komunikatTrue, komunikatFalse);
	}
	//----------------------------enterText
	/**
	 * Metoda wpisujaca tekst do elementu o podanym Id
	 * @param id - id elementu
	 * @param value - wartosc do wpisania
	 */
	public void enterTextById(String id, String value)
	{
		WebElement element = findElementById(id);
		enterText(element, value);
		reportPass("Wprowadzono wartosc '"+value+"' do elementu o id='"+id+"'");		
	}
	/**
	 * Metoda wpisujaca tekst do elementu o podanej sciezce XPath
	 * @param xpath
	 * @param value - wartosc do wpisania
	 */
	public void enterTextByXpath(String xpath, String value)
	{
		WebElement element = findElementById(xpath);
		enterText(element, value);
		reportPass("Wprowadzono wartosc '"+value+"' do elementu o xpath='"+xpath+"'");		
	}
	/**
	 * Metoda wpisujaca tekst do podanego elementu
	 * @param element - WebElement
	 * @param value - wartosc do wpisania
	 */
	private void enterText(WebElement element, String value)
	{
		try
		{
			element.clear();
			element.sendKeys(value);
		}
		catch (NullPointerException npe)
		{
			reportFail("Nie udalo sie wpisac tekstu '"+value+"' do podanego elementu",true);
		}		
	}
	//----------------------------click element
	/**
	 * Metoda klikajaca w element o podanym Id
	 * @param id - id elementu
	 */
	public void clickElementById(String id)
	{
		WebElement element = findElementById(id);
		clickElement(element);
		reportPass("Kliknieto element o id='"+id+"'");		
	}
	/**
	 * Metoda klikajaca w element o podanej sciezce XPath
	 * @param xpath
	 */
	public void clickElementByXPath(String xpath)
	{
		WebElement element = findElementByXpath(xpath);
		clickElement(element);
		reportPass("Kliknieto element o xpath='"+xpath+"'");		
	}
	/**
	 * Metoda klikajaca w element o podanej nazwie
	 * @param name - nazwa elementu
	 */
	public void clickElementByName(String name)
	{
		WebElement element = findElementByXpath("//*[text()='"+name+"']");
		clickElement(element);
		reportPass("Kliknieto element o nazwie='"+name+"'");		
	}
	/**
	 * Metoda klikajaca w podany element
	 * @param element - WebElement
	 */
	public void clickElement(WebElement element)
	{
		try
		{
			element.click();
		}
		catch (NullPointerException npe)
		{
			reportFail("Nie udalo sie kliknac podanego elementu",true);
			reportFail("Komunikat = "+npe,true);
		}
		catch (ElementNotVisibleException enve)
		{
			reportFail("Nie udalo sie kliknac podanego elementu",true);
			reportFail("Komunikat = "+enve,true);
		}	
	}
	//----------------------------radio button
	/**
	 * Metoda wyszukujaca radio button o podanej nazwie
	 * @param name - nazwa elementu
	 * @return WebElement
	 */
	public WebElement radioButtonFindByName(String name)
	{
		WebElement radioButton = findElementByXpath("//*[text()='"+name+"']/preceding-sibling::input[@type='radio']");
		if (radioButton.equals(null))
		{
			reportFail("RadioButton o nazwie '"+name+"' nie zostal odnaleziony",false);
		}
		else
		{
			reportPass("RadioButton o nazwie '"+name+"' zostal odnaleziony");
		}
		return radioButton;
	}
	/**
	 * Metoda klikajaca w radio button o podanej nazwie
	 * @param name - nazwa elementu
	 */
	public void radioButtonClickByName(String name)
	{
		WebElement radioButton=radioButtonFindByName(name);
		try
		{
			radioButton.click();
		}
		catch (Exception e)
		{
			reportFail("Nie udalo sie kliknac Radio button o nazwie '"+name+"'",true);
		}
		reportPass("Zaznaczono RadioButton o nazwie '"+name+"'");
		
	}
	//----------------------------button:
	/**
	 * Metoda wyszukujaca przycisk o podanej nazwie
	 * @param name - nazwa elementu
	 * @return WebElement
	 */
	public WebElement buttonFindByName(String name)
	{
		WebElement button = findElementByXpath("//input[@type='submit' and @value='"+name+"'] | //button[text()='"+name+"'] | //input[@type='button' and @value='"+name+"']");
		if (button.equals(null))
		{
			reportFail("Button o nazwie '"+name+"' nie zostal odnaleziony",false);
		}
		else
		{
			reportPass("Button o nazwie '"+name+"' zostal odnaleziony");
		}
		return button;
	}
	/**
	 * Metoda klikajaca w przycisk o podanej nazwie
	 * @param name - nazwa elementu
	 */
	public void buttonClickByName(String name)
	{
		WebElement button=buttonFindByName(name);
		try
		{
			button.click();
		}
		catch (Exception e)
		{
			System.out.println("Nie udalo sie kliknac przycisku o nazwie '"+name+"'");
			System.out.println("Error = '"+e+"'");
			reportFail("Nie udalo sie kliknac przycisku o nazwie '"+name+"'",true);
		}
		reportPass("Kliknieto przycisk o nazwie '"+name+"'");
		
	}
	//----------------------------wait for element	
	/**
	 * Metoda czekajaca na podany element 'ToBeClickable'
	 * @param element - WebElement
	 * @throws Exception
	 */
	public void waitForElement(WebElement element) throws Exception 
	{
		try 
		{
			WebDriverWait wait = new WebDriverWait(driver, Constant.DEFAULT_TIMEOUT);
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} 
		catch (Exception e) 
		{
			reportFail("Blad przy oczekiwaniu na element", false);			
			reportFail("Komunikat = "+e.getMessage(), false);
		}
	}
	/**
	 * Metoda czekajaca na element o podanym tekscie
	 * @param text - tekst elementu
	 */
	public void waitForElementByText(String text) 
	{
		WebElement element = findElementByXpath("//*[contains(text(),'"+text+"')]");
		try 
		{
			waitForElement(element);
		} 
		catch (Exception e) 
		{			
			reportFail("Blad przy oczekiwaniu na element o tekscie= "+text, false);			
			reportFail("Komunikat = "+e.getMessage(), false);
		}		
	}
	/**
	 * Metoda czekajaca na element o podanej sciezce XPath
	 * @param xpath
	 */
	public void waitForElementByXPath(String xpath) 
	{
		WebElement element = findElementByXpath(xpath);
		try 
		{
			waitForElement(element);
		} 
		catch (Exception e) 
		{			
			reportFail("Blad przy oczekiwaniu na element o xpath= "+xpath, false);			
			reportFail("Komunikat = "+e.getMessage(), false);
		}		
	}
	//----------------------------mainTabLink
	/**
	 * Metoda klikajaca wybrane okno glownej zakladki po jej nazwie
	 * @param name - nazwa elementu - example: "Realizacje"
	 */
	public void clickMainTabLinkByName(String name)
	{
		WebElement mainTabLink = findElementByXpath("//*[contains(@id,'topNav')]/descendant::*[text()='"+name+"'] | //*[contains(@id,'topNav')]/descendant::*[@value='"+name+"']");
		try
		{
			mainTabLink.click();
		}
		catch (Exception e)
		{
			reportFail("Nie udalo sie kliknac zakladki o nazwie '"+name+"'",true);
		}
		reportPass("Kliknieto w zakladke "+name);
	}
	public void hoverMainTabLinkByName(String name)
	{
		WebElement mainTabLink = findElementByXpath("//*[contains(@id,'topNav')]/descendant::*[text()='"+name+"'] | //*[contains(@id,'topNav')]/descendant::*[@value='"+name+"']");
		hoverElement(mainTabLink);
		reportPass("Najechano na zakladke '"+name+"'");
	}
	//----------------------------texfield:
	/**
	 * Metoda wpisujaca podany tekst do elementu textField po znajdujacej sie obok labelce.
	 * @param label - tekst znajdujacy sie obok pola textField (input)
	 * @param value - wartosc do wpisania
	 */
	public void textFieldSetTextByLabel(String label, String value)
	{
		WebElement textField = findElementByXpath("//*[contains(text(),'"+label+"')]/following::input[1]");
		try
		{
			textField.clear();
			textField.sendKeys(value);
		}
		catch (NullPointerException npe)
		{
			reportFail("Nie udalo sie wpisac tekstu '"+value+"' do pola tekstowego o nazwie zawierajacej '"+label+"'",true);
		}
		reportPass("Wpisano tekst '"+value+"' do pola tekstowego o nazwie zawierajacej '"+label+"'");
	}
	
	//---------------------------- link:
	/**
	 * Metoda wyszukujaca link (element a lub input) o podanej nazwie
	 * @param name - nazwa elementu
	 * @return WebElement
	 */
	public WebElement linkFindByName (String name)
	{
		WebElement link = findElementByXpath("//a[text()='"+name+"'] | //input[text()='"+name+"']");
		if (link.equals(null))
		{
			reportFail("Link o nazwie '"+name+"' nie zostal odnaleziony",false);
		}
		else
		{
			reportPass("Link o nazwie '"+name+"' zostal odnaleziony");
		}
		return link;		
	}
	//uwzgledniono opcje podawania konc�wek id (OK)
	/**
	 * Metoda wyszukujaca element po podanym Id.
	 * Uwzgledniono opcje podawania tylko konc�wek Id
	 * @param id - id elementu
	 * @return WebElement
	 */
	public WebElement linkFindByID(String id)
	{
		WebElement link = findElementByXpath("//*[substring(@id,string-length(@id)-string-length('"+id+"')+1,string-length('"+id+"'))='"+id+"']");
		if (link==null)
		{
			reportFail("Link o id='"+id+"' nie zostal odnaleziony",false);
		}
		else
		{
			reportPass("Link o id='"+id+"' zostal odnaleziony");
		}
		return link;	
	}
	/**
	 * Metoda klikajaca w link o podanym Id
	 * @param id - id elementu
	 */
	public void linkClickByID(String id)
	{
		WebElement link=linkFindByID(id);
		try
		{
			link.click();
		}
		catch (Exception e)
		{
			reportFail("Nie udalo sie kliknac linku o id='"+id+"'",true);
		}
		reportPass("Kliknieto w link o id='"+id+"'");
	}
	/**
	 * Metoda klikajaca w link o podanej nazwie
	 * @param name - nazwa elementu
	 */
	public void linkClickByName(String name)
	{
		WebElement link=linkFindByName(name);
		try
		{
			link.click();
		}
		catch (Exception e)
		{
			reportFail("Nie udalo sie kliknac linku o nazwie '"+name+"'",true);
		}
		reportPass("Kliknieto w link o nazwie '"+name+"'");
	}
	//----------------------------is visible
	/**
	 * Metoda sprawdzajaca czy jest widoczny element o podanej sciezce XPath
	 * @param value - xpath do elementu
	 * @return true or false
	 */
	public boolean isElementVisibileByXpath(String value)
	{
		boolean result;
		WebElement element=null;
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
		try
		{			
			element = driver.findElement(By.xpath(value));
		}
		catch(Exception e)
		{
			result=false;
		}	
		if (element==null)
		{
			result=false;
		}
		else
		{
			result=true;
		}
		
		driver.manage().timeouts().implicitlyWait(Constant.DEFAULT_TIMEOUT, TimeUnit.SECONDS);
		return result;
	}
	/**
	 * Metoda sprawdzajaca czy jest widoczny element o podanej nazwie.
	 * @param value - nazwa/wartosc elementu
	 * @return true or false
	 */
	public boolean isElementVisibileByName(String value)
	{
		return isElementVisibileByXpath("//*[contains(text(),'"+value+"')]");
	}
	/**
	 * Metoda sprawdzajaca czy jest widoczny przycisk o podanej nazwie.
	 * @param name - nazwa elementu
	 * @return true or false
	 */
	public boolean isButtonVisibleByName(String name)
	{
		return isElementVisibileByXpath("//input[@type='submit' and @value='"+name+"'] | //button[text()='"+name+"'] | //input[@type='button' and @value='"+name+"']");
	}
	//----------------------------select item from list
	/**
	 * Metoda wybierajaca podany element z listy po Id
	 * @param id - id elementu
	 * @param value - wartosc do wybrania
	 */
	public void selectItemFromListById(String id, String value)
	{
		try
		{
			Select lista = new Select(findElementById(id));
			lista.selectByVisibleText(value);
		}
		catch(Exception e)
		{
			reportFail("Nie udalo sie wybrac elementu '"+value+"' z listy o id='"+id+"'", true);
		}
		reportPass("Wybrano element '"+value+"' z listy o id='"+id+"'");
	}
	/**
	 * Metoda wybierajaca element o podanym indeksie z listy po Id
	 * @param id - id elementu
	 * @param index - numer elementu do wybrania
	 */
	public void selectItemFromListByIdAndIndex(String id, int index)
	{
		try
		{
			Select lista = new Select(findElementById(id));
			lista.selectByIndex(index);
		}
		catch(Exception e)
		{
			reportFail("Nie udalo sie wybrac elementu nr '"+index+"' z listy o id='"+id+"'", true);
		}
		reportPass("Wybrano elementu nr '"+index+"' z listy o id='"+id+"'");
	}
	/**
	 * Metoda wybierajaca element o podanym indeksie z listy po domyslnie wybranym tekscie
	 * @param defaultText - domyslnie wybrany tekst - example: "Wybierz kontakt z listy albo dodaj nowy"
	 * @param value - wartosc do wybrania
	 */
	public void selectItemFromListByDefaultText(String defaultText, String value)
	{
		try
		{
			Select lista = new Select(findElementByXpath("//option[@selected='selected' and contains(@value,'"+defaultText+"')]/parent::select"));
			lista.selectByVisibleText(value);
		}
		catch(Exception e)
		{
			reportFail("Nie udalo sie wybrac elementu '"+value+"' z listy o domyslnej wartosci='"+defaultText+"'", true);
		}
		reportPass("Wybrano element '"+value+"' z listy o domyslnej wartosci='"+defaultText+"'");
	}
	/**
	 * Metoda wybierajaca element o podanym indeksie z listy po domyslnie wybranym tekscie
	 * @param defaultText - domyslnie wybrany tekst - example: "Wybierz kontakt z listy albo dodaj nowy"
	 * @param index - numer elementu do wybrania
	 */
	public void selectItemFromListByDefaultTextAndIndex(String defaultText, int index)
	{
		try
		{
			Select lista = new Select(findElementByXpath("//option[@selected='selected' and contains(@value,'"+defaultText+"')]/parent::select"));
			lista.selectByIndex(index);			
		}
		catch(Exception e)
		{
			reportFail("Nie udalo sie elementu nr '"+index+"' z listy o domyslnej wartosci='"+defaultText+"'", true);
		}
		reportPass("Wybrano elementu nr '"+index+"' z listy o domyslnej wartosci='"+defaultText+"'");
	}
	/**
	 * Metoda wybierajaca podany element z listy po znajdujacej sie obok labelce
	 * @param label - tekst znajdujacy sie obok pola select
	 * @param value - wartosc do wybrania
	 */
	public void selectItemFromListByLabel(String label, String value)
	{
		try
		{
			Select lista = new Select(findElementByXpath("//*[contains(text(),'"+label+"')]/following::select[1]"));
			lista.selectByVisibleText(value);
		}
		catch(Exception e)
		{
			reportFail("Nie udalo sie wybrac elementu '"+value+"' z listy o labelce ='"+label+"'", true);
		}
		reportPass("Wybrano element '"+value+"' z listy o labelce ='"+label+"'");
	}
	/**
	 * Metoda wybierajaca podany element o podanym indeksie z listy po znajdujacej sie obok labelce
	 * @param label - tekst znajdujacy sie obok pola select
	 * @param index - numer elementu do wybrania
	 */
	public void selectItemFromListByLabelAndIndex(String label, int index)
	{
		try
		{
			Select lista = new Select(findElementByXpath("//*[contains(text(),'"+label+"')]/following::select[1]"));
			lista.selectByIndex(index);
		}
		catch(Exception e)
		{
			reportFail("Nie udalo sie wybrac elementu o indeksie '"+index+"' z listy o labelce ='"+label+"'", true);
		}
		reportPass("Wybrano element o indeksie '"+index+"' z listy o labelce ='"+label+"'");
	}
	
	public void selectItemFromSpecialListById(String id, String value)
	{		
		clickElementByXPath("//*[@id='"+id+"']/a/span");	
		if(!isElementVisibileByXpath("//li[contains(text(),'"+value+"')]"))
		{
			clickElementByXPath("//*[@id='"+id+"']/a/span");	
		}
		clickElementByXPath("//li[contains(text(),'"+value+"')]");	
		reportPass("(Wybrano element '"+value+"' z listy spec. o id = '"+id+"')");
	}
	//----------------------------textarea:
	/**
	 * Metoda wpisujaca podany tekst do pola textArea po opisujacej go legendzie
	 * @param legend - tekst pola legend opisujacy obiekt textArea
	 * @param value - wartosc do wpisania 
	 */
	public void textArea_EnterTextByLegend(String legend,String value)
	{
		WebElement textArea = findElementByXpath("//legend[text()='"+legend+"']/following-sibling::textarea[1]");
		try
		{
			textArea.sendKeys(value);
		}
		catch (NullPointerException npe)
		{
			reportFail("Nie udalo sie wpisac tekstu '"+value+"' do pola textArea o nazwie '"+legend+"'",true);
		}
		reportPass("Wpisano tekst '"+value+"' do pola textArea o nazwie '"+legend+"'");
	}
	/**
	 * Metoda wpisujaca podany tekst do pola textArea po ID
	 * @param id - id elementu
	 * @param value - wartosc do wpisania 
	 */
	public void textArea_EnterTextById(String id,String value)
	{
		WebElement textArea = findElementById(id);
		try
		{
			textArea.sendKeys(value);
		}
		catch (NullPointerException npe)
		{
			reportFail("Nie udalo sie wpisac tekstu '"+value+"' do pola textArea o id= '"+id+"'",true);
		}
		reportPass("Wpisano tekst '"+value+"' do pola textArea o id= '"+id+"'");
	}
	//----------------------------checkbox:
	/**
	 * Metoda klikajaca checkbox o podanej labelce
	 * @param label - tekst znajdujacy sie obok checkboxa - example: "Przyznanie adres�w IP:" 
	 */
	public void checkboxClickByLabel(String label)
	{
		WebElement checkBox = findElementByXpath("//*[contains(text(),'"+label+"')]//following::*[@type='checkbox'][1]");
		try
		{
			checkBox.click();
		}
		catch (Exception e)
		{
			reportFail("Nie udalo sie kliknac checkbox o labelce '"+label+"'",true);
		}
		reportPass("Kliknieto checkbox o labelce '"+label+"'");
	}
	/**
	 * Metoda klikajaca checkbox o podanym id
	 * @param id - id elementu
	 */
	public void checkboxClickByID(String id)
	{
		WebElement checkBox = findElementByXpath("//*[@type='checkbox' and substring(@id,string-length(@id)-string-length('"+id+"')+1,string-length('"+id+"'))='"+id+"'][1]");
		try
		{
			checkBox.click();
		}
		catch (Exception e)
		{
			reportFail("Nie udalo sie kliknac checkbox o id '"+id+"'",true);
		}
		reportPass("Kliknieto checkbox o id '"+id+"'");
	}
	//---------------------------- alert:
	public void detectAlert() throws Exception  {
		try 
		{
			Alert alert = driver.switchTo().alert();
			alert.getText();
			reportPass("Wykryto alert: " + alert.getText());
			alert.accept();
		} 
		catch (NoAlertPresentException  e) 
		{
			reportFail("Blad przy wykrywaniu okna alertu", false);
		} 
		catch (Exception  e) 
		{
			reportFail("Blad przy wykrywaniu okna alertu", false);
		}
		//return true;
	}
	public void detectAndCancelAlert() throws Exception  {
		try 
		{
			Alert alert = driver.switchTo().alert();
			alert.getText();
			reportPass("Wykryto alert: " + alert.getText());
			alert.dismiss();
		} 
		catch (NoAlertPresentException  e) 
		{
			reportFail("Blad przy wykrywaniu okna alertu", false);
		} 
		catch (Exception  e) 
		{
			reportFail("Blad przy wykrywaniu okna alertu", false);
		}
		//return true;
	}
	//----------------------------hover element:
	private void hoverElement(WebElement element)
	{
		Actions a = new Actions(driver);	
		try
		{
			a.moveToElement(element).build().perform();        
		}
		catch(Exception e)
		{
			reportFail("Blad przy najechaniu na element:", false);
			reportFail(e.getMessage(), false);
		}
	}
	public void hoverElementByXPath(String xpath)
	{
		WebElement element = findElementByXpath(xpath);
		hoverElement(element);
	}
	//----------------------------
	public int getNumberOfElementsByXPath(String xpath)
	{
		int result=0;
		result=driver.findElements(By.xpath("//*[contains(text(),'"+xpath+"')]")).size();
		return result;
	}
	
	public void waitForPageToLoad()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		int i = 0;
		
		while (!js.executeScript("return document.readyState").toString().equals("complete"))
		{
			try 
            {
                Thread.sleep(1000);
                i++;
            } catch (InterruptedException e) {}
			if(i>Constant.DEFAULT_TIMEOUT)
			{
				break;
			}
		}
	}
	
	
	
	public void quit()
	{
		extent.flush();
		driver.quit();
	}
	//-----------------------
	private void ScrollTo(int xPosition, int yPosition)
	{
	    String js = "window.scrollTo("+xPosition+", "+yPosition+")";
	    JavascriptExecutor jse = (JavascriptExecutor)driver;
	   jse.executeScript(js);
	}	

	public void ScrollToView(WebElement element)
	{
	    if (element.getLocation().y > 100)
	    {
	        ScrollTo(0, element.getLocation().y - 100); // Make sure element is in the view but below the top navigation pane
	    }
	}
	
}
