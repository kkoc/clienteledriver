package utility;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Actions_Call 
{
	public ClienteleDriver cliDriver;
	
	public Actions_Call(ClienteleDriver _cliDriver)
	{
		cliDriver=_cliDriver;
	}
	
	/***
	 * Metoda dodajaca nowy call na zakladce zlecenia techniczne
	 * @param typZlecenia - String - nazwa calla pojawiajaca sie na liscie lub TMPL_TEST_GRZECH2015 dla utworzenia calli z tego szablonu
	 * @param nrUslugi - numer uslugi wedlug kolejnosci na rozwijanej liscie
	 */
	public void addNewCall(String typZlecenia, int nrUslugi)
	{
		cliDriver.linkClickByName("Zlecenia Techniczne");
		if (typZlecenia == "TMPL_TEST_GRZECH2015")
		{
			cliDriver.enterTextByXpath("//*[text()='OSS CID: '][1]/input[1]", "TMPL_TEST_GRZECH2015");
			cliDriver.clickElementByXPath("//*[text()='OSS CID: '][1]/input[2]");
			cliDriver.reportPass("Zgloszenia z szablonu TMPL_TEST_GRZECH2015 zostaly dodane");
		}
		else
		{
			cliDriver.selectItemFromListByDefaultText("Wybierz typ zlecenia", typZlecenia);			
			if (nrUslugi >1) 
			{
				cliDriver.clickElementByXPath("//*[contains(text(),'Wybierz us�ug�')]/parent::div");
				//odznaczenie select all
				if (!cliDriver.findElementByXpath("//*[text()='Select all']/preceding-sibling::input[@type='checkbox']").getAttribute("checked").equals(null))
				{
					cliDriver.clickElementByXPath("//*[text()='Select all']/preceding-sibling::input[@type='checkbox']");
				}
				cliDriver.clickElementByXPath("//div[@id='checks']//span[2]/input[@type='checkbox']["+nrUslugi+"]");
				cliDriver.buttonClickByName("OK");				
			}
			cliDriver.buttonClickByName("Dodaj nowe zlecenie");
			cliDriver.waitForElementByText("Szczeg�y zlecenia");
			cliDriver.verifyElementVisibleByName("Szczeg�y zlecenia", "Zgloszenie "+typZlecenia+" zostalo dodane", "Zgloszenie "+typZlecenia+" NIE zostalo dodane");
			if (cliDriver.isElementVisibileByXpath("//*[@id='_ctl1__ctl0_lblCallNum']"))
			{
				String idCall= cliDriver.findElementById("_ctl1__ctl0_lblCallNum").getText();
				cliDriver.reportPass("Id utworzonego Call'a to: "+idCall);
			}
		}	
	}
	
	/***
	 * Metoda uzupelniajaca adresy A-End i B-End. Obsluguje nowo okno.
	 * @throws Exception
	 */
	private void dodajAdresy_AEnd_BEnd() throws Exception
	{
		//A - End 
				String mwh=cliDriver.driver.getWindowHandle();
				cliDriver.clickElementById("_ctl1__ctl0_Link1_btnAEnd");
				Set<String> s = cliDriver.driver.getWindowHandles(); //this method will gives you the handles of all opened windows
				Iterator<String> ite=s.iterator();
				while(ite.hasNext())		
				{
				    String popupHandle=ite.next().toString();
				    if(!popupHandle.contains(mwh))
				    {
				        cliDriver.driver.switchTo().window(popupHandle);
				        //---actions in new window:
				        cliDriver.selectItemFromListById("ddlCity", "Warszawa");
				        Thread.sleep(1000);
				        cliDriver.selectItemFromListByIdAndIndex("lbAddress", 4);
				        Thread.sleep(1000);
				        cliDriver.buttonClickByName("Wybierz lokalizacj�");
				        //detectAlert();
				        //---After finished your operation in pop-up just select the main window again
				        cliDriver.driver.switchTo().window(mwh);		        
				    }
				}
				cliDriver.buttonClickByName("Zapisz zmiany");
				cliDriver.detectAlert();

				//B - End 
				mwh=cliDriver.driver.getWindowHandle();
				cliDriver.clickElementById("_ctl1__ctl0_Link1_btnBEnd");
				s = cliDriver.driver.getWindowHandles(); //this method will gives you the handles of all opened windows
				ite=s.iterator();
				while(ite.hasNext())		
				{
				    String popupHandle=ite.next().toString();
				    if(!popupHandle.contains(mwh))
				    {
				        cliDriver.driver.switchTo().window(popupHandle);
				        //---actions in new window:
				        cliDriver.selectItemFromListById("ddlCity", "Warszawa");
				        Thread.sleep(1000);
				        cliDriver.selectItemFromListByIdAndIndex("lbAddress", 10);
				        Thread.sleep(1000);
				        cliDriver.buttonClickByName("Wybierz lokalizacj�");		        
				        //---After finished your operation in pop-up just select the main window again
				        cliDriver.driver.switchTo().window(mwh);		        
				    }
				}		
	}
	/**
	 * Metoda wypelniajaca wymagane pola na formularzu calla GENERIC
	 * @param lstWykonawcaValue - example: ".WROCLAW - .WROCLAW FERA"
	 * @param Bandwidth - example: "40000"
	 * @throws Exception
	 */
	public void fillNewCall_Generic(String lstWykonawcaValue, String Bandwidth) throws Exception
	{		
		cliDriver.selectItemFromListByLabel("Wykonawca:", lstWykonawcaValue);
		if (cliDriver.isElementVisibileByName("Dodaj przynajmniej JEDEN Kontakt do Klienta!!!"))
		{
			cliDriver.buttonClickByName("Dodaj kontakt");
			cliDriver.selectItemFromListByDefaultTextAndIndex("Wybierz kontakt z listy albo dodaj nowy.", 1);
			cliDriver.buttonClickByName("Dodaj wybrany");
		}
		dodajAdresy_AEnd_BEnd();
		
		cliDriver.textArea_EnterTextByLegend("Dodatkowe informacje o przebiegu ��cza", "Lorem ipsum dolor sit amet - Dodatkowe informacje o przebiegu lacza");
		cliDriver.textFieldSetTextByLabel("Przepustowo��:", Bandwidth);
		cliDriver.enterTextById("_ctl1__ctl0_Link1__ctl2_txtDescription", "Lorem ipsum dolor sit amet - Tekstowy opis przebiegu polaczenia");

		cliDriver.reportPass("Wymagane pola dla calla GENERIC zostaly wypelnione.");
	}	
	/**
	 * Metoda wypelniajaca wymagane pola na formularzu calla Deinstalacja transmisji (obluga Radio,SDH,OS,Generic)
	 * @param lstWykonawcaValue - example: ".WROCLAW - .WROCLAW FERA"
	 * @throws Exception
	 */
	public void fillNewCall_Deinstallation(String lstWykonawcaValue) throws Exception 
	{
		cliDriver.selectItemFromListByLabel("Wykonawca:", lstWykonawcaValue);
		if (cliDriver.isElementVisibileByName("Dodaj przynajmniej JEDEN Kontakt do Klienta!!!"))
		{
			cliDriver.buttonClickByName("Dodaj kontakt");
			cliDriver.selectItemFromListByDefaultTextAndIndex("Wybierz kontakt z listy albo dodaj nowy.", 1);
			cliDriver.buttonClickByName("Dodaj wybrany");
		}
		dodajAdresy_AEnd_BEnd();
		cliDriver.textArea_EnterTextByLegend("Dodatkowe informacje o przebiegu ��cza", "Lorem ipsum dolor sit amet - Dodatkowe informacje o przebiegu lacza");
		
		cliDriver.reportPass("Wymagane pola dla calla Deinstalacja transmisji (obluga Radio,SDH,OS,Generic) zostaly wypelnione");
	}
	/**
	 *  Metoda wypelniajaca wymagane pola na formularzu calla RADIOLINIA
	 * @param Bandwidth - example: "40000"
	 * @param RLType - example: "Marconi - 28GHz"
	 * @param Port - example: "Ethernet"
	 * @param WspolGeog_Szer - example:"00 00 00.0"
	 * @param WspolGeog_Dlug - example:"00 00 00.0"
	 * @param Subcontractor - example: "Magnus"
	 * @throws Exception
	 */
	public void fillNewCall_RL(String Bandwidth, String RLType, String Port, String WspolGeog_Szer, String WspolGeog_Dlug, String Subcontractor, String Wizja) throws Exception 
	{
		if (cliDriver.isElementVisibileByName("Dodaj przynajmniej JEDEN Kontakt do Klienta!!!"))
		{
			cliDriver.buttonClickByName("Dodaj kontakt");
			cliDriver.selectItemFromListByDefaultTextAndIndex("Wybierz kontakt z listy albo dodaj nowy.", 1);
			cliDriver.buttonClickByName("Dodaj wybrany");
		}
		dodajAdresy_AEnd_BEnd();		
		cliDriver.textArea_EnterTextByLegend("Dodatkowe informacje o przebiegu ��cza", "Lorem ipsum dolor sit amet - Dodatkowe informacje o przebiegu lacza");
		cliDriver.textFieldSetTextByLabel("Przepustowo��:", Bandwidth);
		cliDriver.selectItemFromListByLabel("RL Type:", RLType);
		cliDriver.selectItemFromListByLabel("Port:", Port);
		cliDriver.enterTextById("_ctl1__ctl0_Link1__ctl2_txtWspolGeog_Szer", WspolGeog_Szer);
		cliDriver.enterTextById("_ctl1__ctl0_Link1__ctl2_txtWspolGeog_Dlug", WspolGeog_Dlug);
		cliDriver.selectItemFromListByLabel("Podwykonawca:", Subcontractor); //?
		cliDriver.textFieldSetTextByLabel("Nr Wizji:", Wizja);
		
		cliDriver.reportPass("Wymagane pola dla calla RADIO zostaly wypelnione.");	
	}
	/**
	 * Metoda wypelniajaca wymagane pola na formularzu calla ZUI
	 * @param lstWykonawcaValue - example: ".WROCLAW FERA - .WROCLAW"
	 * @param Bandwidth - example: "128"
	 * @throws Exception
	 */
	public  void fillNewCall_ZUI(String lstWykonawcaValue, String Bandwidth) throws Exception 
	{		
		//Parametry techniczne
		cliDriver.selectItemFromListByLabel("Realizowany przez:", lstWykonawcaValue);
		cliDriver.checkboxClickByLabel("Przyznanie adres�w IP:");
		cliDriver.selectItemFromListByIdAndIndex("_ctl1__ctl0_ddlA_end", 1);
		cliDriver.selectItemFromListByIdAndIndex("_ctl1__ctl0_ddlB_end", 2);
		cliDriver.textFieldSetTextByLabel("Przepustowo��", Bandwidth);
		cliDriver.textArea_EnterTextById("_ctl1__ctl0_Internet_Request_P0_Zaterminowana", "karta_0123456789");	
		//Adresacja IP
		cliDriver.textFieldSetTextByLabel("Abonent:","127.0.0.1/11");
		cliDriver.enterTextById("_ctl1__ctl0_Address_IP1_txtLink_IP_GTS", "127.0.0.1/11");
		
		cliDriver.reportPass("Wymagane pola dla calla ZUI zostaly wypelnione.");
	}
	/**
	 * Metoda wypelniajaca wymagane pola na formularzu calla PBX
	 * @param NrWiodacy
	 * @param dataUruch - example: "2016-12-31"
	 * @param Centrala - example: "BROAD_SOFT" 
	 * @param Usluga - example: "KONTO"
	 * @param Interfejs - example: "Internet"
	 * @param CPE - example: "Bramka"
	 * @throws Exception
	 */
	public  void fillNewCall_PBX(String NrWiodacy, String dataUruch, String Centrala, String Usluga, String Interfejs, String CPE) throws Exception 
	{		
		cliDriver.textFieldSetTextByLabel("Termin uruchomienia", dataUruch);
		cliDriver.textFieldSetTextByLabel("Numer wiod�cy", NrWiodacy);
		//---dziwne poukrywane selecty:		
		selectItemfromPBXListById("centrala_chosen", Centrala);		
		selectItemfromPBXListById("service_chosen", Usluga);		
		selectItemfromPBXListById("interfaceService_chosen", Interfejs);		
		cliDriver.clickElementByXPath("//*[@id='CPE_chosen']/ul");
		cliDriver.clickElementByXPath("//li[text()='Bramka']");
		//---		
		cliDriver.reportPass("Wymagane pola dla calla PBX zostaly wypelnione.");
	}	
	/**
	 * Metoda wyszukujaca Call
	 * @param callID 
	 */
	public void findCall(String callID)
	{
		cliDriver.clickMainTabLinkByName("Realizacje");
		cliDriver.enterTextById("callno", callID);
		cliDriver.clickElementByXPath("//input[@id='callno']/following::button[1]");
		cliDriver.waitForElementByText("Szczeg�y zlecenia");
		cliDriver.verifyElementVisibleByName("Szczeg�y zlecenia", "Zgloszenie o ID: " + callID + " zostalo wyszukane", "Nie znaleziono calla o ID: " + callID);
	}
	
	/**
	 * Metoda usuwajaca wszystkie mozliwe Calle z okna transakcji. 
	 */
	public void deleteCalls()
	{
		try 
		{
			List<WebElement> listDelBtn = cliDriver.driver.findElements(By.xpath("//input[@onclick='btn_Click(this);'][@value='Delete']"));
			Integer count_listDelBtn = listDelBtn.size();
    		if (count_listDelBtn>0) 
    		{
    			for(int i=0;i<count_listDelBtn;i++)
    			{
    				cliDriver.driver.findElement(By.xpath("//input[@onclick='btn_Click(this);'][@value='Delete'][1]")).click();
    				Thread.sleep(1000);
    			}
    			cliDriver.reportPass("Usunieto "+count_listDelBtn+" calli.");    			
    		}
		} 
		catch (Exception e) 
		{
			cliDriver.reportFail("Wystapil blad przy usuwaniu calli", true);
		}
	}
	
	//-----------------
	public void przejmijZgloszenie()
	{
		String przypisanyUser="";
		cliDriver.buttonClickByName("Przejmij Zg�oszenie");
		przypisanyUser=cliDriver.findElementById("_ctl0_lblWorkingUsr").getText();
		if(przypisanyUser.equals(Constant.USERNAME))
		{
			cliDriver.reportPass("Call zostal przejety do edytowania");
		}
		else
		{
			cliDriver.reportFail("Call NIE zostal przejety do edytowania", false);
		}		
	}
	
	/**
	 * Metoda przekazujaca calla wedlug podanego parametru.
	 * @param przekazDo - example: "realizacji", "podwykonawcy", "done/zainstalowano", "zrealizuj" 
	 */
	private void przekazCallDo(String przekazDo)
	{
		String idCall= cliDriver.findElementById("_ctl1__ctl0_lblCallNum").getText();
		String callStatus="";
		
		switch (przekazDo.toLowerCase()) 
		{
			case "realizacji":			
				//---------do realizacji:
				if(cliDriver.isButtonVisibleByName("Do realizacji"))
				{
					cliDriver.buttonClickByName("Do realizacji");
				}
				else if(cliDriver.isButtonVisibleByName("Przeka� do Realizacji"))
				{
					cliDriver.buttonClickByName("Przeka� do Realizacji");
				}
				else if(cliDriver.isButtonVisibleByName("Przekaz do realizacji"))
				{
					cliDriver.buttonClickByName("Przekaz do realizacji");
				}
				callStatus = "DoRealizacji";
				break;
			
			case "podwykonawcy":
				//---------do podwykonawcy:
				if(cliDriver.isButtonVisibleByName("Do podwykonawcy"))
				{
					cliDriver.buttonClickByName("Do podwykonawcy");
				}	
				callStatus = "U_podwykonawcy";
				break;
			
			case "done":
				//---------do done/zainstalowano:
				if(cliDriver.isButtonVisibleByName("Done/Zainstalowano"))
				{
					cliDriver.buttonClickByName("Done/Zainstalowano");
				}
				callStatus = "DONE";
				break;
			
			case "zrealizuj":
				//---------do Zrealizuj:
				if(cliDriver.isButtonVisibleByName("Zrealizuj"))
				{
					cliDriver.buttonClickByName("Zrealizuj");
				}
				callStatus = "Closed";
				
				break;			
				
			default:
				cliDriver.reportFail("Podano zly argument metody 'przekazCallDo'", true);
				break;
		}
					
		//----------sprawdzenie czy pojawilo sie okno bledu:
		if(cliDriver.isElementVisibileByName("Server Error"))
		{
			cliDriver.reportFail("Pojawil sie ekran 'Server Error'", true);
		}
		else
		{
			sprawdzStatusCalla(idCall,callStatus);
		}
		
	}	

	public void sprawdzStatusCalla(String callID, String status)
	{
		findCall(callID);
		String callStatus= cliDriver.findElementById("_ctl1__ctl0_lblCallStatus").getText();
		if(callStatus.toLowerCase().equals(status.toLowerCase()))
		{
			cliDriver.reportPass("Call jest w statusie "+status);
		}
		else
		{
			cliDriver.reportFail("Call NIE jest w statusie "+status, true);
		}
	}
	public void przekazCallDoRealizacji()
	{
		przekazCallDo("realizacji");
	}
	public void przekazCallDoPodwykonawcy()
	{
		przekazCallDo("podwykonawcy");
	}
	public void przekazCallDoDoneZainstalowano()
	{
		przekazCallDo("done");
	}
	public void ZrealizujCall(String typ)
	{
		String idCall= cliDriver.findElementById("_ctl1__ctl0_lblCallNum").getText();
		String callStatus="";
		
		if(cliDriver.isButtonVisibleByName("Zrealizuj"))
		{
			cliDriver.buttonClickByName("Zrealizuj");
		}
		else if(cliDriver.isButtonVisibleByName("Zrealizowano"))
		{
			cliDriver.buttonClickByName("Zrealizowano");
		}
		
		if(typ.equals("RL"))
		{
			callStatus = "ZbudowanoRL";
		}
		else
		{
			callStatus = "Closed";
		}
		
		//----------sprawdzenie czy pojawilo sie okno bledu:
		if(cliDriver.isElementVisibileByName("Server Error"))
		{
			cliDriver.reportFail("Pojawil sie ekran 'Server Error'", true);
		}
		else
		{
			sprawdzStatusCalla(idCall,callStatus);
		}		
	}
	
	public void processCall_PBX()
	{
	
		selectItemfromPBXListById("selectedCallAction_chosen","Przeka� do realizacji ");		
		cliDriver.buttonClickByName("Wykonaj akcje");
		
		cliDriver.buttonClickByName("Przejmij zg�oszenie");
		verifyPrzejecieCalla(1);		
		selectItemfromPBXListById("selectedCallAction_chosen", "Wykonanie allokacji");
		cliDriver.buttonClickByName("Wykonaj akcje");
		
		cliDriver.buttonClickByName("Przejmij zg�oszenie");
		verifyPrzejecieCalla(2);		
		selectItemfromPBXListById("selectedCallAction_chosen", "Wykonanie konfiguracji");
		cliDriver.buttonClickByName("Wykonaj akcje");
		
		cliDriver.buttonClickByName("Przejmij zg�oszenie");
		verifyPrzejecieCalla(3);
		selectItemfromPBXListById("selectedCallAction_chosen", "Konfiguracja IN");
		cliDriver.textArea_EnterTextById("uwagi_Actions", "Zamykanie calla");
		cliDriver.buttonClickByName("Wykonaj akcje");
		
		String sprCzyClosed = "//*[@id='callDetailsShortDiv']/descendant::*[normalize-space(text())='Closed']";
		cliDriver.verifyElementVisibleByXpath(sprCzyClosed, "Status calla = Closed", "Status calla != Closed");
	}
	public void selectItemfromPBXListById(String id, String value)
	{		
		cliDriver.clickElementByXPath("//*[@id='"+id+"']/a/span");	
		if(!cliDriver.isElementVisibileByXpath("//li[text()='"+value+"']"))
		{
			cliDriver.clickElementByXPath("//*[@id='"+id+"']/a/span");	
		}
		cliDriver.clickElementByXPath("//li[text()='"+value+"']");		
	}
	
	
	private void verifyPrzejecieCalla(int number)
	{
		cliDriver.waitForPageToLoad();
		if(cliDriver.getNumberOfElementsByXPath("U�ytkownik "+Constant.USERNAME+" przej�� zg�oszenie")==number)
		{
			cliDriver.reportPass("Przejeto zgloszenie");
		}
		else
		{
			cliDriver.reportFail("Nie udalo sie przejac zgloszenia",true);
		}
	}

}
